FROM node:11-stretch

ENV NODE_ENV production

WORKDIR /app

RUN apt-get update && \
    apt-get install build-essential -y

RUN npm i -g node-gyp

RUN apt-get install mongodb-server -y
RUN mkdir -p /data/db

COPY package*.json yarn.lock ./
RUN yarn
COPY . .

CMD sh docker.sh
