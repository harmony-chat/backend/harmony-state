const MongoDB = require("mongodb");
const fetch = require("node-fetch");
const env = require("./env");
const r = require("./rethink");
const utils = require("./utils");
const constants = require("./constants");

global.Promise = require("bluebird");
Promise.promisifyAll(MongoDB);

const express = require("express");
const notifier = require("./notifier");
const app = express();

// We don't start listening until after mongo connects.
// Again, just another failsafe
MongoDB.connectAsync(`mongodb://${env.mongo.host}:${env.mongo.port}`, {
  useNewUrlParser: true
}).then(mongo => {
  const db = mongo.db(env.mongo.db);

  app.listen(env.port);

  app.use(express.json());

  // Pseudo-map of sorts
  const ALL_KEYS = [["c", "channelId"], ["g", "guildId"], ["u", "userId"]];
  const ALL_KEYS_MAP = new Map(ALL_KEYS);

  const messageBacklog = [];
  const rewindables = new Map([
    [
      "blah",
      new Map([
        [
          "g_1234",
          // Basically just an index of messageBacklog
          0
        ]
      ])
    ]
  ]);

  // Something more advanced will come Eventually tm
  // (If length > N? Other metrics?)
  // Essentially, this needs to exist because our rewindPosition
  // will break otherwise. I suppose we could just have a constantly
  // rolling number but I'm not sure that an always-incrementing number
  // is the best idea
  setInterval(() => janitor(), 10 * 60000);

  function janitor() {
    // Potential memory leaks are not worth the risk
    messageBacklog.length = 0;

    for (const value of rewindables.values()) {
      // Clear out the map at a deeper level just in case
      value.clear();
    }
    // Clear out the remaining tombstones
    rewindables.clear();

    console.log("Janitor task executed with success");
  }

  app.post("/unsubscribe", async function(req, res) {
    const indexes = [];

    console.log(req.body);
    if (req.body.disconnect) {
      await db
        .collection("subscriptions")
        .deleteMany({sessionId: req.body.sessionId});
      await db.collection("sessions").deleteOne({id: req.body.sessionId});
    } else {
      for (const key in req.body.d) {
        for (const itemId of req.body.d[key]) {
          indexes.push(key + "_" + itemId);
        }
      }

      console.log("Index...", indexes);

      await db.collection("subscriptions").deleteMany({
        index: {$in: indexes},
        sessionId: req.body.sessionId
      });
    }

    return res.json({});
  });

  app.post("/subscribe", async function(req, res) {
    const sessionId = req.body.sessionId;
    const subscriptions = [];
    const response = {};
    const ourRewindables = rewindables.get(sessionId);
    console.log("ourRewindables...", ourRewindables);
    const dispatches = [];
    console.log("Subscribing, what do we have?", req.body);
    for (const subscriptionKey in req.body.d) {
      const subscriptionLongKey = ALL_KEYS_MAP.get(subscriptionKey);
      console.log(subscriptionKey);
      for (const subscription of req.body.d[subscriptionKey]) {
        const index = subscriptionKey + "_" + subscription;
        subscriptions.push({
          itemId: subscription,
          key: subscriptionKey,
          sessionId,
          // For faster lookups we precalculate these
          index,
          id: sessionId + "_" + subscriptionKey + "_" + subscription
        });
        if (ourRewindables) {
          const rewindablePosition = ourRewindables.get(index);
          console.log("rewindablePosition", rewindablePosition);
          if (response[subscriptionKey]) {
            response[subscriptionKey][subscription] =
              rewindablePosition !== undefined;
          } else {
            response[subscriptionKey] = {
              [subscription]: rewindablePosition !== undefined
            };
          }
          if (rewindablePosition !== undefined) {
            const batches = new Map();
            console.log("HUzzah!");
            for (
              let i = rewindablePosition > 0 ? rewindablePosition : 0;
              i < messageBacklog.length;
              i++
            ) {
              console.log("Rewinding step", i, messageBacklog[i]);
              const event = messageBacklog[i];
              if (!event) {
                throw new Error(
                  "Somehow no event..? len:" +
                    messageBacklog.length +
                    "pos:" +
                    rewindablePosition +
                    "i:" +
                    i
                );
              }
              let determinedKey = null;
              for (const [shortKey, longKey] of ALL_KEYS) {
                if (event.d[longKey] !== undefined) {
                  determinedKey = longKey;
                  break;
                }
              }
              if (!determinedKey) {
                console.warn("Somehow we have no key for event?", event);
                continue;
              }
              if (
                determinedKey != subscriptionLongKey ||
                event.d[subscriptionLongKey] != subscription
              ) {
                // Just not an event we're interested in...
                continue;
              }
              const underIndex = event.t.lastIndexOf("_");
              const storeType = event.t.substring(0, underIndex);
              const storeEvent = event.t.substring(underIndex + 1);
              if (storeEvent == "DELETE") {
                batches.set(storeType, event);
              } else {
                const batch = batches.get(storeType);
                if (batch) {
                  for (const key in event) {
                    batch[key] = event[key];
                  }
                } else {
                  batches.set(storeType, event);
                }
              }
            }
            for (const batchItem of batches.values()) {
              dispatches.push(batchItem);
            }
          }
          console.log(
            "OURREWINDABLES SETTING",
            index,
            messageBacklog.length - 1
          );
          ourRewindables.set(index, messageBacklog.length - 1);
        } else {
          console.log(
            "CREATING OURREWINDABLES",
            sessionId,
            index,
            messageBacklog.length - 1
          );
          rewindables.set(
            sessionId,
            new Map([[index, messageBacklog.length - 1]])
          );
        }
      }
    }
    await db
      .collection("sessions")
      .replaceOne(
        {id: sessionId},
        {id: sessionId, host: req.body.hostAddress, userId: req.body.u},
        {upsert: true}
      );

    console.log("Subscribing to...", subscriptions);
    await db.collection("subscriptions").insertMany(subscriptions);

    if (dispatches.length) {
      await fetch(req.body.hostAddress, {
        method: "POST",
        redirect: "follow",
        body: JSON.stringify({
          r: [sessionId],
          d: dispatches.length == 1 ? dispatches[0] : dispatches
        })
      });
    }

    return res.json(response);
  });

  app.post("/session", async function(req, res) {
    await db.collection("sessions").replaceOne(
      {id: req.body.s},
      {
        id: req.body.s,
        host: req.body.h,
        userId: req.body.u
      },
      {upsert: true}
    );
    return res.status(204).send();
  });

  app.post("/disconnect", async function(req, res) {
    console.log("Disconnecting!");
    const sessionCount = await db
      .collection("sessions")
      .find({userId: req.body.u, id: {$ne: req.body.s}})
      .count();
    if (sessionCount === 0) {
      console.log("No more sessions exist, going offline...");
      await r
        .table("users")
        .get(req.body.u)
        .update({
          activity: null,
          presence: constants.presence.offline
        })
        .run(r.conn);
      await dispatchMessage({
        t: "USER_UPDATE",
        d: {
          id: req.body.u,
          userId: req.body.u,
          activity: null,
          presence: constants.presence.offline
        }
      });
    } else {
      console.log(
        `Not disconnecting session... Still ${sessionCount} remaining`
      );
    }

    // Not needed any longer
    await db.collection("sessions").removeOne({id: req.body.s});
    await db.collection("subscriptions").removeMany({sessionId: req.body.s});

    res.status(204).send();
  });

  app.post("/message", async function(req, res) {
    await dispatchMessage(req.body);
    res.status(204).send();
  });

  async function dispatchMessage(payload) {
    console.log("ONLY PUSH()");
    messageBacklog.push(payload);
    console.log(messageBacklog);
    let itemId = null;
    let key = null;
    for (const [subscriptionKey, eventKey] of ALL_KEYS) {
      if (payload.d[eventKey]) {
        itemId = payload.d[eventKey];
        key = subscriptionKey;
        break;
      }
    }
    const subscriptions = await db.collection("subscriptions").find({
      index: key + "_" + itemId
    });

    const flattenPromises = new Set();
    const flattenedHosts = new Map();

    const notMentioningUsers = new Set();

    console.log("Going to dispatch payload", payload);

    for await (const subscription of subscriptions) {
      console.log("Found subscription...", subscription);
      const promise = db
        .collection("sessions")
        .findOne({
          id: subscription.sessionId
        })
        .then(session => {
          notMentioningUsers.add(session.userId);
          console.log("We got a session...", session);
          const existingFlattened = flattenedHosts.get(session.host);
          if (existingFlattened) {
            existingFlattened.add(subscription.sessionId);
          } else {
            flattenedHosts.set(session.host, new Set([subscription.sessionId]));
          }
        });
      flattenPromises.add(promise);
      console.log("bla");
    }

    console.log("Done iterating... Awaiting all promises");
    await Promise.all(flattenPromises.values());

    // TODO: Turn this into more of a blacklist of sorts instead so we don't wait on this to dispatch
    if (payload.t == "MESSAGE_CREATE") {
      if (payload.d.authorId) notMentioningUsers.add(payload.d.authorId);
      const messagePayload = {
        type: "MESSAGE",
        data: {
          ...payload.m,
          content: payload.d.content
        }
      };
      if (payload.m.mentionsEveryone) {
        r
          .table("members")
          .getAll(payload.m.guildId, {index: "guildId"})
          .eqJoin("userId", r.table("users"))
          .zip()
          .filter(function(doc) {
            return doc("presence").ne(constants.presence.dnd);
          })
          .do(function(doc) {
            return doc("userId");
          })
          .run(r.conn)
          .then(async memberCursor => {
            for await (const memberId of utils.cursorIterator(memberCursor)) {
              console.log("Member", member);
              if (!notMentioningUsers.has(userId)) {
                notifier.sendNotification(memberId, messagePayload);
              }
            }
          });
      } else {
        const matches = payload.d.content.match(/<@\d+>/g);

        if (matches) {
          for (const match of matches) {
            const userId = match.slice(2, -1);
            if (!notMentioningUsers.has(userId)) {
              r
                .table("members")

                .get(`${userId}-${payload.m.guildId}`)
                .do(function(doc) {
                  return r.and(
                    doc.ne(null),
                    r
                      .table("users")
                      .get(userId)("presence")
                      .ne(constants.presence.dnd)
                  );
                })
                .run(r.conn)
                .then(userExists => {
                  if (userExists) {
                    return notifier.sendNotification(userId, messagePayload);
                  }
                });
            }
          }
        }
      }
    }

    console.log("flattenPromises()");
    const dispatchPromises = new Set();
    console.log("About to look at our flattendHosts", flattenedHosts);
    for (const [host, sessions] of flattenedHosts.entries()) {
      // We don't ever call .json() because the response should be like a 204 or similar
      dispatchPromises.add(
        fetch(host + "/message", {
          method: "POST",
          body: JSON.stringify({
            d: {
              t: payload.t,
              d: payload.d
            },
            r: Array.from(sessions.values()),
            h:
              "http://" +
              (process.env.HOSTNAME || "localhost") +
              ":" +
              (process.env.PORT || "3002")
          }),
          // Just in case
          redirect: "follow"
        })
      );
    }
    await Promise.all(dispatchPromises.values());
    console.log("Finished dispatching");
  }
});
