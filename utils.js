function cursorIterator(cursor) {
  return {
    [Symbol.asyncIterator]() {
      return {
        cursor,
        async next() {
          console.log("cursor.next()");
          const value = await this.cursor
            .next()
            // I have literally no idea why I have to do this this way
            .catch(
              err =>
                err.name == "ReqlDriverError" &&
                err.message == "No more rows in the cursor."
                  ? null
                  : Promise.reject(err)
            );
          if (value) {
            return {
              value,
              done: false
            };
          } else {
            return {done: true};
          }
        }
      };
    }
  };
}
module.exports = {cursorIterator};
