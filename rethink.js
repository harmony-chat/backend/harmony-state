const r = require("rethinkdb");
const env = require("./env");
const connWaiter = r
  .connect({
    db: env.rethink.db,
    host: env.rethink.host,
    port: env.rethink.port,
    user: env.rethink.user,
    password: env.rethink.password,
    optionalRun: false
  })
  .then(conn => {
    module.exports.conn = r.conn = conn;
  });

module.exports = r;
module.exports.connWaiter = connWaiter;
