const webPush = require("web-push");
const r = require("./rethink");
const env = require("./env");
const utils = require("./utils");

module.exports = {
  async sendNotification(user, data) {
    const payload = JSON.stringify({data});
    console.log("Sending off payload..", payload);
    const deviceCursor = await r
      .table("devices")
      .getAll(user, {index: "ownerId"})
      .filter({enabled: true})
      .run(r.conn);

    for await (const device of utils.cursorIterator(deviceCursor)) {
      console.log("Posting to device", device.id);
      try {
        await webPush.sendNotification(
          device.subscriptionData,
          payload || "EGG",
          {
            vapidDetails: {
              publicKey: env.vapid.publicKey,
              privateKey: env.vapid.privateKey,
              subject: "https://gitlab.com/harmony-chat/backend/harmony-state"
            }
          }
        );
      } catch (err) {
        console.error(err);
      }
    }
  }
};
