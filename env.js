module.exports = {
  mongo: {
    db: process.env.MONGO_DB || "harmony-state",
    host: process.env.MONGO_HOST || "localhost",
    port: process.env.MONGO_PORT || "27017"
  },
  rethink: {
    host: process.env.RETHINK_HOST || "localhost",
    port: process.env.RETHINK_PORT || 28015,
    user: process.env.RETHINK_USER || "admin",
    password: process.env.RETHINK_PASSWORD || "",
    get db() {
      return `harmony_${module.exports.deployment}`;
    }
  },
  vapid: {
    publicKey:
      process.env.VAPID_PUBKEY ||
      "BC5kwKgoeEBL1ona8pBX6ZQjetS9fS3xObhE09SKJjXEXKqs5RYoUwi9T_dj5xpuIYwFV6BLF-VEVJ_Zp9Kb500",
    privateKey:
      process.env.VAPID_PRIVKEY || "CdwcfXT-JF8tTwrcC3xu04JPGU71PuU-fhRx2K9dW0M"
  },
  deployment: process.env.NODE_ENV || "local",
  port: 3002
};
